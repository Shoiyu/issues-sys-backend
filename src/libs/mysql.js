const knex = require('knex')
require('dotenv').config()

const mysql = knex({
    client: 'mysql',
    connection: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWD,
        database: process.env.MYSQL_DB,
        timezone: 'UTC',
        dateStrings: true
    }
})

console.log(`New mysql connection on PORT: ${process.env.PORT}`)

module.exports = mysql
