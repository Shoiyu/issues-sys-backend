const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core')
const { ApolloServer } = require('apollo-server-express')
const context = require('./contextCreation')
const typeDefs = require('./types')
const resolvers = require('./resolvers')


module.exports = new ApolloServer({
    resolvers,
    typeDefs,
    context,
    cors: {
        allowedHeaders: ["Accept", "Content-Type", "Authorization"],
    },
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
    introspection: true,
  
})

