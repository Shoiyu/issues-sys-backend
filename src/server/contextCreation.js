
const { AuthenticationError } = require("apollo-server-express");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = ({req}) => {
    let context = {}

    let authHeader = req.headers['authorization']
    if(authHeader){
        const splitHeader = authHeader.split(" ")
        if(splitHeader[0] != "Bearer"){
            throw new AuthenticationError('Invalid auth scheme')
        }
        try {
            const decodedUser = jwt.verify(splitHeader[1], process.env.JWT_SECRET)
            context.payload = decodedUser
            
        } catch (e) {
            throw new AuthenticationError('invalid Token')
        }
    }
    return context
}