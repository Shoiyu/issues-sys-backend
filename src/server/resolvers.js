
const userTypes = require('../modules/user')
const projectTypes = require('../modules/project')
const issueTypes = require('../modules/issue')

const queries = {
        ...userTypes.queries,
        ...projectTypes.queries,
        ...issueTypes.queries
    }
const mutations = {
        ...userTypes.mutations,
        ...projectTypes.mutations,
        ...issueTypes.mutations
    }
    
module.exports = {
    Query: queries,
    Mutation: mutations
}