const  gql  = require('graphql-tag')

module.exports = gql`

    schema {
        query: Query,
        mutation: Mutation
    }

    scalar Datetime

    type Query
    type Mutation


`
