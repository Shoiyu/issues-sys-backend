
const userTypes = require('../modules/user').def
const projectTypes = require('../modules/project').def
const issueTypes = require('../modules/issue').def

const types = [
    userTypes,
    projectTypes,
    issueTypes
]

module.exports = types