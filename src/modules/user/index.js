const { gql } = require('apollo-server')
const userResolvers = require('./resolvers')

const def = gql `
    type User {
        id: Int
        name: String
        surname: String
        email: String
    }

    type Query{
        getMe: User
    }

    // type Token {
    //     confirmed_email: Boolean!
    //     token: String
    // }

    // type Mutation{
    //     register(name: String!, surname: String!, email: String!, password: String!): Token
    //     login(email: String!, password: String!): Token
    // }
`

const queries = {
    getMe : userResolvers.getMe
}

// const mutations = {
//     login: userResolvers.mutations,
//     register: userResolvers.register
// }

module.exports = { def, queries }
