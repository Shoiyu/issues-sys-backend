const mysql = require('../../libs/mysql')

const getUserById = (id) => {
    return mysql.select("*").from('user').where('id', id).first()
}
module.exports = {
    getUserById
}