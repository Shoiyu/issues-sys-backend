const { UserInputError, AuthenticationError } = require('apollo-server')
const jwt = require("jsonwebtoken")

const userModel = require('./model')



const getMe = async (root, args, context) => {
    if(!context.payload || !context.payload.id){
        throw new AuthenticationError('User not logged in')
    }
    const user  = await userModel.getUserById(context.payload.id)
    return user
}

module.exports = {
    getMe
}