require('dotenv').config()
const express = require('express')
const http = require('http')
const server = require('./server')

const app = express()
server.applyMiddleware({ app })
const httpServer = http.createServer(app)


httpServer.listen(process.env.PORT, () => {
    console.log(`Server ready at PORT: http://localhost:${process.env.PORT}`)
})
